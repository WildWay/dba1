# Postgres. DBA1.

Конспекты и задачи курса ["Postgres DBA1"](https://postgrespro.ru/education/courses/DBA1) от Postgres Pro

---

На случай, если ссылки будут не доступны, слайды лекций и некоторые важные материалы загружены в директории с темами.

---
## Введение
### [Тема 00. Введение](00_Introduction/00_Introduction.md)

---
## Базовый инструментарий
### [Тема 01. Установка и управление сервером](01_Server_installation_and_management/01_Server_installation_and_management.md)
### [Тема 02. Использование psql](02_Using_psql/02_Using_psql.md)
### [Тема 03. Конфигурирование PostgreSQL](03_PostgreSQL_configuring/03_PostgreSQL_configuring.md)

---
## Архитектура
### [Тема 04. Общее устройство PostgreSQL](04_PostgreSQL_architecture/04_PostgreSQL_architecture.md)
### [Тема 05. Изоляция и многоверсионность](05_Isolation_and_MVCC/05_Isolation_and_MVCC.md)
### [Тема 06. Буферный кэш и журнал](06_Cache_and_WAL/06_Cache_and_WAL.md)

---
## Организация данных
### [Тема 07. Базы данных и схемы](07_Databases_and_schemas/07_Databases_and_schemas.md)
### [Тема 08. Системный каталог](08_System_catalog/08_System_catalog.md)
### [Тема 09. Табличные пространства](09_Tablespaces/09_Tablespaces.md)
### [Тема 10. Низкий уровень](10_Low_level/10_Low_level.md)

---
## Задачи администрирования
### [Тема 11. Мониторинг](11_Monitoring/11_Monitoring.md)
