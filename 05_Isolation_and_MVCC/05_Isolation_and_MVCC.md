# [Назад, к оглавлению](../README.md)

# Тема 05. Изоляция и многоверсионность

## Материалы:
1. [Практика многоверсионность](https://edu.postgrespro.ru/dba1/dba1_05_arch_mvcc.html)
1. [Практика уровень изоляции](https://edu.postgrespro.ru/dba1/dba1_05_arch_mvcc_lab.html)

---

## Тезисы:
1. ### Блокировки:
    1. Строк:
        - Чтение никогда не блокирует строки
        - Читающие транзакции не блокируют друг друга
        - Изменение строки блокирует её для изменения, но не для чтения
    1. Таблиц:
        - Запрещают изменение или удаление таблицы, пока с ней идёт работа
        - Запрещают чтение таблицы при перестроении/перемещении/т.п.
    1. Время жизни блокировок:
        - Устанавливаются транзакциями автоматически, по мере необходимости, или вручную
        - Снимаются автоматически, при завершении транзакции
1. ### Транзакции:
    1. Статус:
        - Определяется в специальных файлах, на диске (pg_xact)
        - Два бита на транзакцию:
            - Бит транзакция зафиксирована
            - Бит транзакция прервана
        - Если транзакция прервана - выполняется откат
        - Если не зафиксирована и не прервана - транзакция активна (выполняется)
1. ### Очистка:
    1. Старые версии строк хранятся вместе с актуальными
        - Изменения увеличивают размер таблиц и индексов
    1. Процесс очистки (vacuum):
        - Удаляет версии строк, которые уже не нужны (не видны ни в одном снимке данных)
        - Работает параллельно с другими процессами, в случае если блокирует кого-то - приостанавливает свою работу
        - удалённые версии оставлют в файлах данных "дыры", которые используются для новых версий строк
    1. Полная очистка:
        - Блокирует таблицу на время работы
        - Полностью перестраивает файлы данных, делая их компактными
1. ### Автоочистка:
    1. Autovacuum launcher:
        - Фоновый процесс
        - Реагирует на активность изменения данных
    1. Autovacuum worker:
        - Запускается launcher'ом по необходимости 
        - Выполняет очистку
1. ### [Уровни изоляции](https://en.wikipedia.org/wiki/Isolation_(database_systems)):
    1. Read uncommited - не поддерживается PostgreSQL
        - позволяет читать не зафиксированные данные
    1. Read commited - используется по умолчанию
        - снимок строится на момент начала оператора
        - одинаковые запросы могут каждый раз получать разные данные
    1. Repeatable read
        - снимок строится на момент начала первого оператора транзакции
        - транзакция может завершиться с ошибкой сериализации
    1. Serializable
        - полная изоляция, но дополнительные накладные расходы
        - транзакция может завершиться с ошибкой сериализации

---

## Итоги:
1. В файлах данных могут храниться несколько версий каждой строки (сначала в буфере)
1. Транзакции работают со снимком данных - согласованным срезом на определённый момент времени
1. Писатели не блокируют читателей, читатели не блокируют никого
1. Момент создания снимка влияет на уровень изоляции
1. Версии строк накапливаются, поэтому нужна периодическая очистка

---

## [Практика](#Материалы:):
1. Создайте таблицу с одной строкой
1. Начните первую транзакцию и выполните запрос к таблице
1. Во втором сеансе удалите строку и зафиксируйте изменения
1. Сколько строк увидит первая транзакция, выполнив тот же запрос повторно?
1. Завершите первую транзакцию
1. Повторите тоже самое на уровне изоляции repeatable read
    - BEGIN ISOLATION LEVEL REPEATABLE READ;
