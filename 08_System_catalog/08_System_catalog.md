# [Назад, к оглавлению](../README.md)

# Тема 08. Системный каталог

## Тезисы:
1. ### Системный каталог:
    - набор таблиц и представлений, описывающих все объекты кластера баз данных
    - основная схема - pg_catalog
    - альтернативное представление - information_schema (стандарт SQL)
1. ### SQL-доступ в системный каталог:
    - просмотр: SELECT
    - изменение: CREATE, ALTER, DROP
1. ### Команды psql для работы с системным каталогом:
    - начинаются с \d
1. ### Схема pg_catalog создаётся в каждой БД
1. ### Есть общие объекты кластера, к которым можно обратиться из любой БД
1. ### Правила именования:
    - __pg__* - общий префикс всех объектов (pg_database, pg_tables, etc)
    - __dat__* - префикс столбцов (обычно повторяет имя объекта - datname, datctype, datallowconn, etc)
    - названия объектов всегда хранятся в нижнем регистре
1. ### Специальные типы данных
    - [OID](https://postgrespro.ru/docs/postgresql/10/datatype-oid.html) - тип для идентификатора объекта
        - первичные и внешние ключи в таблицах системного каталога
        - скрытый столбец, в запросах надо указывать явно
    - Reg-типы
        - псевдонимы OID для некоторых таблиц системного каталога (regclass для pg_class, regtype для pg_type и т.п.)
        - приведение текстового имени объекта к типу OID и обратно
        - пример:
            ```sql
            SELECT *
            FROM pg_attribute
            WHERE attrelid = (SELECT oid FROM pg_class WHERE relname = 'mytable');
            ```
            можно написать как
            ```sql
            SELECT *
            FROM pg_attribute
            WHERE attrelid = 'mytable'::regclass;
            ```
        - Преобразователь вводимого значения типа regclass находит таблицу согласно заданному пути поиска схем, так что он делает «всё правильно» автоматически
1. ### Системный каталог [pg_class](https://postgrespro.ru/docs/postgresql/10/catalog-pg-class)
    - хранит описание целого ряда объектов:
        - таблицы
        - представления (включая материализованные)
        - индексы
        - последовательности
    - Все эти объекты называются в PostgreSQL общим словом "отношение" (relation), отсюда и префикс "rel" в названии столбцов:
        - relname (имя таблицы, индекса, представления и т.п.)
        - relkind (тип объекта)
        - relnamespace (OID схемы)
        - ...


---

## Итоги:
1. ### Системный каталог - метаинформация о кластере в самом кластере
1. ### Таблицы системного каталога:
    - часть хранится в базах данных
    - часть - общая для всего кластера
1. ### Системный каталог использует специальные типы данных

---
## Ссылки:
1. [Системные каталоги](https://postgrespro.ru/docs/postgresql/10/catalogs)

---

## Практика:
1. ### Создать базу данных и тестовые объекты для изучения
    ```sql
    CREATE DATABASE data_catalog;

    \c data_catalog

    CREATE TABLE employees(id serial PRIMARY KEY, name text, manager integer);

    CREATE VIEW top_managers AS
        SELECT * FROM employees WHERE manager IS NULL;
    ```
1. ### Посмотреть описание ранее изученных объектов (БД и схемы)
    ```sql
    SELECT * FROM pg_database WHERE datname = 'data_catalog' \gx

    SELECT * FROM pg_namespace WHERE nspname = 'public' \gx
    ```
1. ### Вывести описание объектов из системного каталога pg_class по маске '^(emp|top).*'
    ```sql
    SELECT relname, relkind, relnamespace, relfilenode, relowner, reltablespace
    FROM pg_class
    WHERE relname ~ '^(emp|top).*';
    ```
1. ### Вывести данные о таблицах схемы public из системного каталога pg_tables
    ```sql
    SELECT schemaname, tablename, tableowner, tablespace
    FROM pg_tables WhERE schemaname = 'public';
    ```
1. ### Вывести данные о представлениях схемы public из системного каталога pg_views
    ```sql
    SELECT * 
    FROM pg_views WHERE schemaname = 'public';
    ```
1. ### OID и reg-типы. Системные каталоги pg_class, pg_attribute
    ```sql
    -- Вывести OID таблицы "employees"
    SELECT oid FROM pg_class WHERE relname = 'employees';

    -- Вывести имена и id типов данных из системного каталога pg_attribute
    SELECT a.attname, a.atttypid
    FROM pg_attribute a;

    -- Отфильтровать вывод предыдущего запроса, используя attrelid (OID таблицы которой принадлежит объект)
    SELECT a.attname, a.atttypid
    FROM pg_attribute a
    WHERE a.attrelid = (
        SELECT oid FROM pg_class WHERE relname = 'employees'
    );

    -- Отфильтровать вывод предыдущего запроса, скрыв системные типы, с помощью выражения attnum > 0 (системные столбцы, обычно, имеют отрицательные номера)
    SELECT a.attname, a.atttypid
    FROM pg_attribute a
    WHERE a.attrelid = (
        SELECT oid FROM pg_class WHERE relname = 'employees'
    )
    AND a.attnum > 0;

    -- Используя reg-класс запрос можно написать проще, без явного обращения к pg_class
    SELECT a.attname, a.atttypid
    FROM pg_attribute a
    WHERE a.attrelid = 'employees'::regclass
    AND a.attnum > 0;

    -- Аналогично, с помощью reg-типа, можно вывести OID типа как текстовое значение
    SELECT a.attname, a.atttypid::regtype
    FROM pg_attribute a
    WHERE a.attrelid = 'employees'::regclass
    AND a.attnum > 0;
    ```
1. ### Информационные команды psql
    ```sql
    -- \d? отображает информацию по "отношениям", где "?" = тип объекта (relkind)
    -- с идентификатором "S" можно добавить к выводу системные объекты
    -- с идентификатором "+" можно получить больше информации
    -- Примеры:
    --- таблицы
    \dt
    \dt+
    \dtS+

    --- представления
    \dv
    \dv+
    \dvS+

    --- схемы
    \dn
    \dn+
    \dnS+

    -- функции
    \df
    \df+
    \dfS+

    -- с помощью шаблонов (спец символов регулярных выражений) можно ограничить выборку
    \df pg*size

    -- посмотреть описание функции
    \sf pg_table_size

    -- получить информацию о конкретном объекте можно с помощью "\d имяОбъекта"
    -- с идентификатором "+" можно вывести дополнительную информацию об объекте
    \d top_managers
    \d+ top_managers

    -- полный список можно найти с помощью команды
    \?
    -- секция Informational
    /Info
    ```
1. ### Вывести запрос, выполняемый при вызове команды psql
    ```sql
    -- Отобразить запросы, выполняемые командами psql, можно задав переменной "ECHO_HIDDEN" значение "on"
    \set ECHO_HIDDEN on

    -- выполнить команду psql, к примеру \dt
    \dt
    -- отобразится секция *** QUERY ***

    -- Отключить отображение запросов, выполняемых командами psql
    \unset ECHO_HIDDEN
    ```
