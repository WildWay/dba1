# [Назад, к оглавлению](../README.md)

# Тема 09. Табличные пространства

## Тезисы:
1. ### Табличное пространство (далее ТП):
    1. каталог, в котором хранятся данные
    1. содержит объекты (все или явно указанные) одной и более баз
1. ### ТП, создаваемые по умолчанию:
    1. __pg_global__ - содержит общие, для кластера, объекты системного каталога
    1. __pg_default__ - ТП объектов по умолчанию
1. ### Для создания нового табличного пространства необходимо:
    1. Создать `${tp_catalog}` (каталог не должен находится в $PGDATA)
        ```bash
        mkdir /home/postgres/ts_dir
        ```
    1. Назначить владельцем пользователя _postgres_
        ```bash
        chown postgres:postgres /home/postgres/ts_dir
        ```
    1. Выполнить запрос
        ```sql
        CREATE TABLESPACE ts LOCATION '/home/postgres/ts_dir';
        ```
1. ### Расположение ТП:
    1. __pg_global__ - ${PGDATA}/global
    1. __pg_default__ - ${PGDATA}/base/${dboid}
    1. __pg_custom__ - ${PGDATA}/pg_tblspc/${tsoid} (симлинк на) => /${tp_catalog}/var/${dboid}
        - ТП необхомо создавать за пределами каталога ${PGDATA}
1. ### ТП для баз и таблиц можно указывать при создании
1. ### Базы и объекты баз (таблицы, индексы и другие объекты) можно перемещать в другое ТП
    - перемещаемые объекты будут заблоикрованы на время перемещения в другую директориию
1. ### ТП можно задать для шаблона по умолчанию (или другого шаблона)
    - в этом случае новая БД и все её объекты будут создаваться сразу в нужном ТП

---

## Итоги:
1. ### Табличные пространства (ТП) - средство для организации физического хранения данных
1. ### Логическое (базы данных, схемы) и физическое (табличные пространства) разделения данных независимы

---

## Практика:
1. ### Создать директорию для табличного пространства
    ```bash
    mkdir -p /home/postgres/ts_dir
    chown postgres:postgres /home/postgres/ts_dir
    ```
1. ### Создать ТП и таблицы в ТП, переместить объекты в другое ТП, удалить ТП
    ```sql
    -- Создать табличное пространство
    -- Обязательно указать абсолютный путь
    CREATE TABLESPACE ts LOCATION '/home/postgres/ts_dir';

    -- Получить список текущих ТП
    SELECT * FROM pg_tablespace;

    -- Получить список текущих ТП и их расположение
    \db

    -- Создать БД appdb в ТП ts
    -- ts станет ТП по умолчанию для всех объектов БД
    CREATE DATABASE appdb TABLESPACE ts;

    -- Подключиться к БД appdb
    \c appdb

    -- Создать таблицу t1
    CREATE TABLE t1(id serial, name text);

    -- Вывести список таблиц и их ТП
    SELECT tablename,tablespace FROM pg_tables WHERE schemaname='public';
    -- пустое табличное пространство = пространство по умолчанию

    -- Создать таблицу t2 в ТП pg_default
    CREATE TABLE t2(n numeric) TABLESPACE pg_default;

    -- Ещё раз вывести список таблиц и их ТП
    SELECT tablename,tablespace FROM pg_tables WHERE schemaname='public';
    -- увидим что pg_default является ТП для t2

    -- Создать БД configdb (ТП по умолчанию) и подключиться к ней
    CREATE DATABASE configdb;
    \c configdb

    -- Создать таблицу t в ТП ts и вывести информацию о таблицах и их ТП
    CREATE TABLE t(n integer) TABLESPACE ts;
    SELECT tablename,tablespace FROM pg_tables WHERE schemaname='public';

    -- Переместить таблицу t в ТП pg_default (таблица будет заблокирована)
    ALTER TABLE t SET TABLESPACE pg_default;


    -- Попробовать удалить ТП ts
    DROP TABLESPACE ts;
    -- сделать это не получится, т.к. в ТП ts есть данные

    -- Получить список баз, содержащих объекты в ТП ts
    SELECT OID AS tsoid FROM pg_tablespace WHERE spcname = 'ts' \gset

    SELECT datname
    FROM pg_database
    WHERE OID IN (SELECT pg_tablespace_databases(:tsoid));

    -- Переключиться в базу appdb
    \с appdb
    -- Переместить все объекты из ТП ts в ТП pg_default
    -- Данная команда выполняется для примера
    -- Действие имет смысл если ТП из которого перемещаем (ts) не является ТП по-умолчанию для БД
    ALTER TABLE ALL IN TABLESPACE ts SET TABLESPACE pg_default;

    -- Получить список баз, содержащих объекты в ТП ts
    SELECT datname
    FROM pg_database
    WHERE OID IN (SELECT pg_tablespace_databases(:tsoid));
    -- Не смотря на перемещение "всех" объектов мы увидим базы, которые были созданы в ТП ts

    -- Получить объекты в ТП по умолчанию
    SELECT * FROM pg_class WHERE reltablespace = 0;
    -- Этот запрос покажет таблицы системного каталога, создающиеся по-умолчанию вместе с БД

    -- Вернуть все объекты в ТП ts
    ALTER TABLE ALL IN TABLESPACE pg_default SET TABLESPACE ts;
    -- Т.к. переместить БД целиком можно только в том случае, если все объекты находятся в ТП по-умолчанию

    -- Переместить БД целиком в другое ТП
    \c postgres
    ALTER DATABASE appdb SET TABLESPACE pg_default;

    -- Убедимся что в ТП ts нет БД
    SELECT datname
    FROM pg_database
    WHERE OID IN (SELECT pg_tablespace_databases(:tsoid));

    -- Удалить ТП ts
    DROP TABLESPACE ts;

    -- Убедиться что ТП ts пропало из списка ТП
    \db
    ```
1. ### Убедиться что в /home/postgres/ts_dir нет данных
    ```bash
    ls -l /home/postgres/ts_dir
    ```
