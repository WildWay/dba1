# [Назад, к оглавлению](../README.md)

# Тема 00. Введение

## Материалы:
1. [Курс](https://postgrespro.ru/education/courses/DBA1)
1. [Видео](https://www.youtube.com/watch?v=mXA861YV7Us&list=PLaFqU3KCWw6JhHBp07QSu9uE8zahhKnTn)
1. [Слайды](https://edu.postgrespro.ru/dba1/dba1_00_introduction.pdf)
1. [Руководство слушателя](https://edu.postgrespro.ru/dba1/dba1_student_guide.pdf)
1. [Учебные материалы](https://edu.postgrespro.ru/DBA1-handouts.zip)

---

## Подготовка:
1. Установить VirtualBox >= v5.1
1. Создать виртуальную машину из образа
    - [Образ для начала занятий](https://edu.postgrespro.ru/DBA1-student1.ova)
    - [Образ с установленным PostgreSQL server](https://edu.postgrespro.ru/DBA1-student2.ova)
