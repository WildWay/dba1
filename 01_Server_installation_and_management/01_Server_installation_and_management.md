# [Назад, к оглавлению](../README.md)

# Тема 01. Установка и управление сервером

Архив с исходными кодами postgresql-10 находится в директории текущей темы.

---

## Материалы:
1. [Учебные материалы](https://edu.postgrespro.ru/DBA1-handouts.zip)

---

## Практика:
1. ### Собрать и установить PostgreSQL server из исходных кодов
1. ### Инициализировать БД, запустить PostgreSQL server, проверить работоспособность
1. ### Собрать и установить модули из исходных кодов, вывести список установленных модулей
1. ### Остановить сервер PostgreSQL

---

## Выполнение:
1. ### Собрать и установить PostgreSQL server из исходных кодов:
    1. `cd ~/`
    1. `tar -xzvf postgresql-10.0.tar.gz`
    1. `cd postgresql-10.0`
    1. `./configure`
    1. `make -j $(grep 'model name' /proc/cpuinfo | wc -l)`
    1. `sudo make -j $(grep 'model name' /proc/cpuinfo | wc -l) install`
1. ### Инициализировать БД, Запустить PostgreSQL server, проверить работоспособность:
    1. `sudo su - postgres` 
    1. `initdb -k`
    1. `pg_ctl -D /usr/local/pgsql/data -l ~/logfile start`
    1. `psql -c 'select now();`
1. ### Собрать и установить модули из исходных кодов, вывести список установленных модулей:
    1. Установка конкретного модуля на примере pgcrypto:
        1. `logout` (выход из сеанса пользователя postgres)
        1. `cd postgresql-10.0/contrib/pgcrypto/` (под пользователем системы)
        1. `make -j $(grep 'model name' /proc/cpuinfo | wc -l)`
        1. `sudo su - postgres` 
        1. `psql -c 'SELECT name,comment FROM pg_available_extensions ORDER BY name'`
    1. Установка всех доступных модулей:
        1. `logout` (выход из сеанса пользователя postgres)
        1. `cd postgresql-10.0/contrib/` (под пользователем системы)
        1. `make -j $(grep 'model name' /proc/cpuinfo | wc -l)`
        1. `sudo su - postgres` 
        1. `psql -c 'SELECT name,comment FROM pg_available_extensions ORDER BY name'`
1. ### Остановить сервер PostgreSQL:
    1. `pg_ctl stop [-m fast]`
