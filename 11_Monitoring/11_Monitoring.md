# [Назад, к оглавлению](../README.md)

# Тема 11. Мониторинг

## Тезисы:

[pgss]: https://postgrespro.ru/docs/postgresql/10/pgstatstatements
[pgst]: https://postgrespro.ru/docs/postgresql/10/pgstattuple
[pgbc]: https://postgrespro.ru/docs/postgresql/10/pgbuffercache
[log_what]: https://postgrespro.ru/docs/postgresql/10/runtime-config-logging#RUNTIME-CONFIG-LOGGING-WHAT
[log_where]: https://postgrespro.ru/docs/postgresql/10/runtime-config-logging#RUNTIME-CONFIG-LOGGING-WHERE
[pgBadger]: https://pgbadger.darold.net/

1. ### Мониторинг может осуществляться как встроенными средствами ОС и postgres, так и сторонними решениями
1. ### Мониторинг средствами ОС:
    1. Процессы - ps (grep postgres)
    1. Использование ресурсов - iostat, vmstat, sar, top, ...
    1. Дисковое пространство - df, du, quota, ...
1. ### Сторонние решения:
    1. Универсальные системы:
        - Prometheus + Grafana, Zabbix, Munin, Cacti
        - в облаке - Okmeter, NewRelic, Datadog
    1. Системы мониторинга PostgreSQL:
        - PGObserver
        - PoWA (PostgreSQL Workload Analyzer)
        - OPM (Open PostgreSQL Monitoring)
1. ### Статистика внутри базы:
    1. Текущие активности системы
    1. Процесс сбора статистики
    1. Дополнительные расширения
1. ### Текущие активности системы:
    - параметр системы - track_activities (включен по умолчанию)
    - представление pg_stat_activity. Содержит:
        - все текущие процессы (клиентские и серверные)
        - их состояние
        - подробные данные по каждому процессу
1. ### Сбор статистики (stats collector):
    параметр        | состояние по-умолчанию | статистика
    --------------- | ---------------------- | ----------
    track_counts    | включен                | обращения к таблицам и индексам (доступы, затронутые строки). Нужен для автоочистки
    track_functions | выключен               | вызовы пользовательских функций
    track_io_timing | выключен               | обращения к страницам
1. ### Некоторые расширения (extensions) статистики в стандартной поставке postgres:
    расширение                 | описание
    -------------------------- | ---------------------- 
    [pg_stat_statements][pgss] | статистика по запросам
    [pgstattuple][pgst]        | статистика по версиям строк
    [pg_buffercache][pgbc]     | состояние буферного кэша
1. ### Некоторые дргие расширения (extensions) статистики:
    расширение         | описание
    ------------------ | ---------------------- 
    pg_stat_plans      | статистика по планам запросов
    pg_stat_kcache     | статистика по процессору и вводу-выводу
    pg_qualstats       | статистика по предикатам
1. ### Для сбора статистики также используется __журнал сервера__
1. ### Сбор статистики с помощью журнала сервера предполагает настройку:
    1. Журнальных записей (тип, уровень) и их формата
    1. Ротацию файлов журнала
    1. Анализа журнала
1. ### Некоторые параметры настройки журнальных записей:
    1. Приёмник сообщений (log_destination = список):
        - stderr - стандартный поток вывода ошибок
        - csvlog - формат CSV (только с logging_collector)
        - syslog - демон syslog
        - eventlog - журнал событий Windows
    1. Коллектор сообщений (logging_collector = on):
        - позволяет собирать дополнительную информацию
        - никогда не теряет сообщения (в отличие от syslog)
        - записывает stderr и csvlog в log_directory/log_filename
    1. [Параметры расширения журнальных записей][log_what]:
        параметр                   | описание
        -------------------------- | ---------------------------------------
        application_name           | имя приложения
        log_connections            | подключения
        log_disconnections         | отключения
        log_statement              | текст выполняемых команд
        log_min_messages           | сообщения определённого уровня
        log_min_duration_statement | время выполнения команд выше значения N
        log_duration               | время выполнения команд
        log_checkpoints            | контрольные точки
        log_lock_waits             | длинные ожидания
        log_temp_files             | использование временных файлов
1. ### Ротация файлов журнала:
    1. [С помощью коллектора сообщений][log_where]:
        параметр                   | описание
        -------------------------- | ---------------------------------------
        log_filename               | маска имени файла
        log_rotation_age           | время ротации
        log_rotation_size          | разер файла для ротации
        log_truncate_on_rotation   | перезаписывать ли файлы
    1. Внешние средства:
        - logrotate
        - rotatelogs
1. ### Анализ журнала:
    1. Средства ОС
        - grep
        - awk
    1. Специальные средства анализа
        - [pgBadger][pgBadger]

---

## Итоги:
1. ### Мониторинг заключается в контроле работы сервера:
    1. Со стороны ОС
    1. Со стороны БД
1. ### PostgreSQL предоставляет собираемую статистику и журнал сообщений сервера
1. ### Для получения наиболее полной информации необходима настройка внешних систем:
    1. Мониторинга
    1. Анализа логов

---

## Практика:
1. ### Имитация нагрузки и анализ внутри СУБД:
    ```sql
    -- Включение сбора статистики ввода вывода
    ALTER SYSTEM SET track_io_timing = on;

    -- Включение сбора статисики выполнения функций
    ALTER SYSTEM SET track_functions = 'all';

    -- Перечитывание конфигурации
    SELECT pg_reload_conf();

    -- Создание базы для тестов
    CREATE DATABASE test_load;

    -- Создание тестового набора таблиц и наполнение данными
    \! pgbench -i test_load

    -- Подключение к базе для тестов
    \c test_load

    -- Сброс статистики
    SELECT pg_stat_reset();
    SELECT pg_stat_reset_shared('bgwriter');

    -- Запуск теста с имитацией нагрузки
    \! pgbench -T 10 test_load

    -- Запуск VACUUM для таблицы pgbench_accounts (ручной аналог autovacuum)
    VACUUM pgbench_accounts;

    -- Вывод статистики обращения к таблице pgbench_accounts в терминах строк:
    SELECT * FROM pg_stat_all_tables WHERE relid='pgbench_accounts'::regclass \gx

    -- Вывод статистики обращения к таблице pgbench_accounts в терминах страниц:
    SELECT * FROM pg_statio_all_tables WHERE relid='pgbench_accounts'::regclass \gx

    -- Вывод статистики обращения к индексам:
    SELECT * FROM pg_stat_all_indexes WHERE relid='pgbench_accounts'::regclass \gx

    SELECT * FROM pg_statio_all_indexes WHERE relid='pgbench_accounts'::regclass \gx

    -- Глобальная статистика по всей базе
    SELECT * FROM pg_stat_database WHERE datname='test_load' \gx

    -- Статистика по процессам фоновой записи и контрольной точке
    SELECT * FROM pg_stat_bgwriter \gx
    ```

1. ### Анализ текущих активностей на примере блокировки:
    ```sql
    -- Сценарий, в котором один процесс блокирует выполнение другого

    -- СЕАНС 1. Создание блокирующеей транзакции
    -- Подключение к БД для тестов
    \c test_load

    -- Создание таблицы и вставка значения
    CREATE TABLE t(n integer);

    INSERT INTO t VALUES(42);

    -- Создание блокирующей транзакции
    BEGIN;
    UPDATE t SET n = n + 1;

    -- СЕАНС 2. Создание заблокированной транзакции
    -- Попытка изменить ту же строку
    UPDATE t SET n = n + 2;

    -- СЕАНС 3. Анализ
    -- Вывод информации об обслуживающих процессах
    SELECT
        pid,
        query,
        state,
        wait_event,
        wait_event_type,
        pg_blocking_pids(pid)
    FROM pg_stat_activity
    WHERE backend_type = 'client backend' \gx
    -- Cостояние idle in transaction означает, что сеанс начал транзакцию, но в настоящее время ничего не делает, а транзакция осталась незавершенной.
    -- Начиная с версии 9.6 в арсенале администратора появился параметр:
    -- idle_in_transaction_session_timeout - принудительно завершает сеансы, в которых транзакция простаивает больше указанного времени

    -- Завершение блокирующего сеанса вручную
    
    -- Сохранение в переменную pid блокирующей транзакции
    SELECT pid as blocked_pid
    FROM pg_stat_activity
    WHERE backend_type = 'client backend'
    AND cardinality(pg_blocking_pids(pid)) > 0 \gset

    -- Завершение pid блокирующей транзакции
    SELECT pg_terminate_backend(b.pid)
    FROM unnest(pg_blocking_pids(:blocked_pid)) AS b(pid);
    -- Функция unnest нужна, поскольку pg_blocking_pids возвращает массив идентификаторов процессов, блокирующих искомый серверный процесс.
    -- В нашем примере блокирующий процесс один, но в общем случае их может быть несколько.

    -- Ещё раз выведем информацию о процессах
    SELECT
        pid,
        query,
        state,
        wait_event,
        wait_event_type,
        pg_blocking_pids(pid)
    FROM pg_stat_activity
    WHERE backend_type = 'client backend' \gx
    -- pid процесса, находящегося в состояние 'idle in transaction' больше нет
    -- процесс, который был им заблокирован, успешно выполнил транзакцию
    ```

1. ### Взаимоблокировка (Deadlock):
    ```sql
    -- Сценарий, в котором процессы блокируют друг друга

    -- Создание таблицы и наполнение данными
    CREATE TABLE dlck_test(n numeric);
    INSERT INTO dlck_test VALUES (1),(2);

    -- СЕССИЯ 1. Транзакция блокирует 1ую строку
    BEGIN;
    UPDATE dlck_test SET n=10 WHERE n=1;

    -- СЕССИЯ 2. Транзакция блокирует 2ую строку и пытается изменить 1ую
    BEGIN;
    UPDATE dlck_test SET n=200 WHERE n=2;

    -- СЕССИЯ 1. Попытка изменить 2ую строку
    UPDATE dlck_test SET n=20 WHERE n=2;

    -- СЕССИЯ 2. Попытка изменить 1ую строку
    UPDATE dlck_test SET n=100 WHERE n=1;
    -- Получаем ошибку взаимоблокировки
    -- ERROR:  deadlock detected

    -- Увидеть количество deadlocks в конкретной БД можно по соответствующему полю запроса
    SELECT * FROM pg_stat_database WHERE datname='test_load' \gx
    ```
