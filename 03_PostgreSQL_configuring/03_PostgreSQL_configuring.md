# [Назад, к оглавлению](../README.md)

# Тема 03. Конфигурирование PostgreSQL

## Тезисы:
### __postgresql.conf__ - основной файл конфигурации
1. Расположение:
    - `SHOW config_file;` - вывести путь задействованного файла конфигурации
    - при сборке по умолчанию - в __$PGDATA/postgresql.conf__
1. При изменении перечитывание конфига выполняется:
    - `pg_ctl reload`
    - `kill -HUP postgres_pid`
    - `SELECT pg_reload_conf();`
### __postgresql.auto.conf__ - файл конфигурации, управляемый SQL. Считывается последним (приоритетные настройки)
1. Расположение - __$PGDATA/postgresql.auto.conf__
1. Управление __postgresql.auto.conf__:
    - `ALTER SYSTEM SET параметр TO значение;` - добавляет или изменяет параметр = значение
    - `ALTER SYSTEM RESET параметр;` - удаление записи параметра
    - `ALTER SYSTEM RESET ALL` - удаление записей всех параметров
1. При изменении перечитывание выполняется аналогично __postgresql.conf__
### __Просмотр параметров__
1. `SELECT * FROM pg_file_settings;` - текущие значения __postgresql.conf__
1. `SELECT * FROM pg_settings;` - текущие значения параметров
1. `SELECT * FROM pg_settings WHERE name = 'work_mem' \gx` - вывести подробную информацию о состоянии параметра
    - __name__, __setting__, __unit__ - название и значение параметра
    - __boot_val__ - значение по-умолчанию
    - __reset_val__ - если параметр был изменён, то командой RESET восстановится это значение
    - __source__ - источник значения параметра
    - __sourceline__ - строка (еслли это файл)
    - __pending_restart__ - ожидается ли рестарт для применения параметра
    - __context__ - действия, необходимые для применения параметра:
        - __internal__ - изменить нельзя, задано при установке
        - __postmaster__ - требуется перезапуск сервера
        - __sighup__ - требуется перечитать файлы конфигурации
        - __superuser__ - суперпользователь может изменить для своего сеанса
        - __user__ - любой пользователь может изменить для своего сеанса
### __Просмотр, изменение и сброс параметров сеанса__
1. `SHOW 'параметр';` - вывести значение параметра
1. `SET параметр TO 'значение';` - установить значение
1. `SELECT set_config('параметр', 'значение', [true/false]);` - установить значение
    - true/false - на транзакцию/до конца сеанса
1. `RESET 'параметр';` - сбросить значение параметра

---

## Практика и выполнение:
1. ### Получите список параметров (и их значений),для изменения которых требуется перезапуск сервера.
    - `SELECT name, setting, unit FROM pg_settings WHERE context = 'postmaster';`
2. ### В файле postgresql.conf установите для параметра listen_addresses значение «*».
    - `\! sed "s/.*listen_addresses =.*/listen_addresses = '*'/" -i /usr/local/pgsql/data/postgresql.conf`
    - `SELECT * FROM pg_file_settings WHERE name = 'listen_addresses'\gx`
    - `SELECT name, setting, pending_restart FROM pg_settings WHERE name = 'listen_addresses';`
3. ### Примените изменения в системе и убедитесь, что новые значения вступили в силу.
    - `\q`
    - `pg_ctl restart`
    - ` SHOW listen_addresses;`
