# [Назад, к оглавлению](../README.md)

# Тема 02. Использование psql

---

## Практика:
1. ### Подключиться к PostgreSQL серверу с помощью psql, вывести информацию о текущем подключении
1. ### Вывести информацию о текущем подключении, справку по командам, выйти из консоли
1. ### Настроить выполнение команд при запуске psql, вывести историю команд
1. ### Настроить формат отображения результата запросов
1. ### Взаимодействие с ОС. Выполнить из psql команды в системе (через shell)
1. ### Записать результат запроса в файл
1. ### Подготовить и выполнить файл запросов
1. ### Установить и вывести переменные среды
1. ### Настроить переменную с выводом топ5 баз по объему
1. ### Настроить постраничный просмотр
1. ### Настроить вывод итогового времени выполнения команд
1. ### Настроить вывод информации о роли в стркое приглашения(ввода)

---

## Выполнение:
1. ### Подключиться к PostgreSQL серверу с помощью psql, вывести информацию о текущем подключении:
    1. `psql --help` - вывести кратку справку по параметрам psql
    1. `man psql` - краткая документация по psql
    1. Пример: __psql -d база -U роль(пользователь) -h узел(хост) -p порт__
    1. `psql -U postgres -d postgres -h localhost -p 5432`
1. ### Вывести информацию о текущем подключении, справку по командам, выйти из консоли:
    1. `\conninfo` - информация о текущем подключении
    1. `\?` - список команд psql
    1. `\? variables` - переменные psql
    1. `\h[elp]` - список команд psql
    1. `\h SELECT` - __\h команда__ синтаксис команды
    1. `\q[uit]` - выход
1. ### Настроить выполнение команд при запуске psql, вывести историю команд:
    1. ~/.psqlrc - файл, с командами, которые будут выполнены при старте psql
    1. `echo "\conninfo" >> ~/.psqlrc` - выводить информацию о подключении при запуске psql
    1. `cat ~/.psql_history` - история команд введённых в psql
1. ### Настроить формат отображения результата запросов:
    1. `\?`, __/Formatting__ - справка по форматированию
    1. Для включения/включения - ввести одну или несколько опций форматирования
    1. `\a \t` - без выравнивания, без служебных строк (только данные)
    1. `\x` - расширенный вывод (когда строки не влезают по ширине)
    1. `\pset` - текущие настройки форматирования
    1. `\pset expanded on` - пример включения __\x__ с помощью __\pset__
1. ### Взаимодействие с ОС. Выполнить из psql команды в системе (через shell):
    1. __\\! команда__ - пример выполнения команды в shell из psql
    1. `\! uptime`, `\! whoami`, `\! pwd` - вывод команд `uptime`, `whoami`, `pwd`
1. ### Записать результат запроса в файл:
    1. `\o ~/psql_output`
    1. `SELECT schemaname, tablename, tableowner FROM pg_tables;` - запрос запишется в файл __~/psql_output__
    1. `\! cat ~/psql_output` - посмотреть содержимое __~/psql_output__
    1. `\o` - вернуть вывод в консоль
1. ### Подготовить и выполнить файл запросов:
    1. Изунтри psql:
        1. `\a \t`
        1. `\pset fieldsep ' '`
        1. `\o query_file`
        1. `SELECT 'SELECT '''||tablename||': ''|| count(*) FROM '|| tablename||';' FROM pg_tables LIMIT 3;`
        1. `\! cat query_file`
        1. `\o`
        1. `\i query_file`
    1. Из shell:
        1. `psql < query_file`
        1. `psql -f query_file`
    1. Выполнение строк запроса из psql:
        1. `SELECT 'SELECT '''||tablename||': ''|| count(*) FROM '|| tablename||';' FROM pg_tables LIMIT 3; \gexec` - выведет запросы и выполнит
        1. `SELECT 'SELECT '''||tablename||': ''|| count(*) FROM '|| tablename||';' FROM pg_tables LIMIT 3 \gexec`  - только выполнит
1. ### Установить и вывести переменные среды:
    1. `\set VAR_NAME var_value_1234` - установить переменную VAR_NAME
    1. `\echo :VAR_NAME` - вывести (не shell)
    1. `\unset VAR_NAME` - убрать переменную VAR_NAME
    1. `SELECT now() AS curr_time \gset` - строго нижний регистр
    1. `\echo :curr_time`
    1. `\set` - вывести переменные среды
1. ### Настроить переменную с выводом топ5 баз по объему:
    1. `echo "\set top5 'SELECT tablename, pg_total_relation_size(schemaname||''.''||tablename) AS bytes, pg_size_pretty(pg_total_relation_size(schemaname||''.''||tablename)) AS pretty_size FROM pg_tables ORDER BY bytes DESC LIMIT 5;'" >> ~/.psqlrc`
    1. `SELECT datname, pg_database_size(datname) AS db_size_bytes, pg_size_pretty(pg_database_size(datname)) AS db_size_pretty FROM pg_database ORDER BY db_size_bytes DESC;`
    1. `SELECT pg_size_pretty(sum(pg_database_size(datname))) AS total_db_size FROM pg_database;`
    1. `psql`, `:top5`
1. ### Настроить постраничный просмотр:
    1. `psql`, `SELECT * FROM pg_tables;`, `\q` - без постраничного просмотра
    1. `echo "\setenv PAGER 'less -XS'" >> ~/.psqlrc` - постраничный просмотр
    1. `psql`, `SELECT * FROM pg_tables;`, `\q` - тоже самое с постраничным просмотром
1. ### Настроить вывод итогового времени выполнения команд:
    1. `echo "\timing on" >> ~/.psqlrc`
    1. `psql`, `SELECT * FROM pg_tables;`, `\q`
1. ### Настроить вывод информации о роли в стркое приглашения(ввода):
    1. `echo "\set PROMPT1 '%n@%/%R%# '" >> ~/.psqlrc`
    1. `echo "\set PROMPT2 '%n@%/%R%# '" >> ~/.psqlr`
    1. `psql`, `\q` - появится информация о роли(пользователе) в строке приглашения
